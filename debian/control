Source: frogr
Section: graphics
Priority: optional
Maintainer: Alberto Garcia <berto@igalia.com>
Build-Depends: debhelper-compat (= 13),
               meson,
               libgtk-3-dev,
               libglib2.0-dev,
               libgstreamer1.0-dev,
               libjson-glib-dev,
               libsoup-3.0-dev,
               libxml2-dev,
               libexif-dev,
               yelp-tools,
               libgcrypt20-dev
Standards-Version: 4.6.2
Rules-Requires-Root: no
Homepage: https://wiki.gnome.org/Apps/Frogr
Vcs-Browser: https://salsa.debian.org/berto/frogr
Vcs-Git: https://salsa.debian.org/berto/frogr.git

Package: frogr
Architecture: any
Depends: frogr-data (= ${source:Version}),
         shared-mime-info,
         ${shlibs:Depends},
         ${misc:Depends}
Recommends: gstreamer1.0-plugins-base,
            gstreamer1.0-plugins-good,
            yelp
Suggests: gstreamer1.0-plugins-bad,
          gstreamer1.0-plugins-ugly
Description: Flickr Remote Organizer for GNOME
 Frogr is a small application for the GNOME desktop that allows users
 to manage their accounts in the Flickr image hosting website.
 It supports all the basic tasks, including uploading pictures, adding
 descriptions, setting tags and managing sets.

Package: frogr-data
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Recommends: frogr
Description: Flickr Remote Organizer for GNOME - data files
 Frogr is a small application for the GNOME desktop that allows users
 to manage their accounts in the Flickr image hosting website.
 It supports all the basic tasks, including uploading pictures, adding
 descriptions, setting tags and managing sets.
 .
 This package contains the architecture-independent data files.
