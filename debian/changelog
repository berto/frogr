frogr (1.8.1-1) unstable; urgency=medium

  * New upstream release.

 -- Alberto Garcia <berto@igalia.com>  Wed, 03 Jan 2024 12:18:40 +0100

frogr (1.8-1) unstable; urgency=medium

  * New upstream release (Closes: #1059823).
  * debian/control:
    - Build depend on libsoup3 instead of v2.
    - Update standards version to 4.6.2, no changes needed.
  * debian/copyright:
    - Update copyright years.

 -- Alberto Garcia <berto@igalia.com>  Wed, 03 Jan 2024 01:02:05 +0100

frogr (1.7-1) unstable; urgency=medium

  * New upstream release (Closes: #1005518).
  * Remove all patches, they're already included in this release.
  * debian/copyright:
    - Update copyright years and use https for the Source: URL.
  * debian/upstream/metadata:
    - Remove noise from one of the lines.
  * Update standards version to 4.6.0.1, no changes needed.

 -- Alberto Garcia <berto@igalia.com>  Wed, 16 Feb 2022 11:45:51 +0100

frogr (1.6-3) unstable; urgency=medium

  [ Alberto Garcia ]
  * replace-warnlevel.patch:
    - Replace obsolete 'warnlevel' option in Meson (Closes: #998590).
  * debian/watch:
    - Set version to 4 (fixes older-debian-watch-file-standard).

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster:
    + Build-Depends: Drop versioned constraint on libexif-dev, libglib2.0-dev,
      libgtk-3-dev, libjson-glib-dev, libsoup2.4-dev and libxml2-dev.
    + frogr-data: Drop versioned constraint on frogr in Replaces.
    + frogr-data: Drop versioned constraint on frogr in Breaks.
  * Bump debhelper from old 10 to 13.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update standards version to 4.5.1, no changes needed.

 -- Alberto Garcia <berto@igalia.com>  Fri, 05 Nov 2021 01:09:45 +0100

frogr (1.6-2) unstable; urgency=medium

  * debian/control:
    - Make frogr-data Multi-Arch: foreign.
    - Update Standards-Version to 4.5.0 (no changes).
  * disable-werror.patch:
    - Don't build with -Werror. Compilation warnings can be caused by the
      build dependencies and Frogr cannot control that.

 -- Alberto Garcia <berto@igalia.com>  Thu, 30 Jan 2020 17:13:12 +0100

frogr (1.6-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
    - Update Standards-Version to 4.4.1 (no changes).
    - Add Rules-Requires-Root: no.
  * debian/copyright:
    - Update copyright years.

 -- Alberto Garcia <berto@igalia.com>  Sat, 11 Jan 2020 19:55:49 +0100

frogr (1.5-1) unstable; urgency=medium

  * New upstream release.
  * debian/compat:
    - Set debhelper compatibility level to 10.
  * debian/control:
    - Add Vcs-* fields.
    - Update Standards-Version to 4.2.1 (no changes).
  * debian/rules:
    - Stop passing --parallel and --buildsystem=meson to dh.
    - Build with hardening=+bindnow.
  * debian/frogr.install:
    - The appstream file now goes to /usr/share/metainfo.
  * debian/copyright:
    - Use https for the format URL.
    - Update copyright years.

 -- Alberto Garcia <berto@igalia.com>  Tue, 27 Nov 2018 09:55:12 +0200

frogr (1.4-1) unstable; urgency=medium

  * New upstream release.
  * Use Meson instead of autotools:
    - debian/control: Replace build dependency on dh-autoreconf with
      meson and require debhelper >= 10.3.
    - debian/rules: Replace '--with autoreconf' with '--buildsystem=meson'.
  * debian/frogr.install:
    - Add usr/share/appdata and remove usr/share/pixmaps.

 -- Alberto Garcia <berto@igalia.com>  Mon, 08 Jan 2018 15:43:40 +0200

frogr (1.3-1) unstable; urgency=medium

  * New upstream release.
  * debian/copyright:
    - Update copyright years.

 -- Alberto Garcia <berto@igalia.com>  Fri, 14 Jul 2017 16:10:09 +0200

frogr (1.1-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
    - Remove build dependency on intltool.
    - Update Standards-Version to 3.9.8 (no changes).
  * debian/copyright:
    - Update copyright years.
  * debian/watch:
    - Use https://download.gnome.org/

 -- Alberto Garcia <berto@igalia.com>  Wed, 05 Oct 2016 09:38:31 +0300

frogr (1.0-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
    - Update build dependency on glib to 2.44.

 -- Alberto Garcia <berto@igalia.com>  Thu, 31 Dec 2015 00:17:28 +0100

frogr (0.11-2) unstable; urgency=medium

  * Release to unstable.

 -- Alberto Garcia <berto@igalia.com>  Mon, 27 Apr 2015 12:24:24 +0300

frogr (0.11-1) experimental; urgency=medium

  * New upstream release.
  * debian/control:
    - Build using libgcrypt20-dev instead of libgcrypt11-dev.
    - Update home page address.
    - Update Standards-Version to 3.9.6 (no changes).
  * debian/copyright:
    - Update copyright years and source address.

 -- Alberto Garcia <berto@igalia.com>  Thu, 08 Jan 2015 09:59:29 +0200

frogr (0.10-1) unstable; urgency=medium

  * New upstream release.
  * Remove fix-yelp-macro.patch, it's already included in this release.
  * debian/watch:
    - Update regular expression to match version 0.10 and higher.
  * debian/control:
    - Remove build dependency on libsoup-gnome2.4-dev.

 -- Alberto Garcia <berto@igalia.com>  Tue, 17 Jun 2014 10:47:35 +0300

frogr (0.9-2) unstable; urgency=medium

  * fix-yelp-macro.patch: fix FTBFS due to incorrect usage of the
    YELP_HELP_INIT macro (Closes: #746057).
  * debian/rules: enable parallel builds.

 -- Alberto Garcia <berto@igalia.com>  Mon, 28 Apr 2014 12:34:49 +0300

frogr (0.9-1) unstable; urgency=low

  * New upstream release.
  * Remove all patches, they're already included in this release.
  * debian/control:
    - Update build dependency on libsoup2.4-dev to >= 2.34.
    - Replace build dependency on gnome-doc-utils with yelp-tools.
    - Update Standards-Version to 3.9.5 (no changes).
  * debian/frogr-data.install:
    - Help files were moved from /usr/share/gnome to /usr/share/help.
  * debian/copyright:
    - Update copyright years.

 -- Alberto Garcia <berto@igalia.com>  Fri, 17 Jan 2014 09:18:59 +0200

frogr (0.8-4) unstable; urgency=low

  * Build using GStreamer 1.0:
    - gstreamer-1.0.patch: port to GStreamer 1.0.
    - debian/control: change all gst dependencies.
  * keywords.patch: add Keywords field to the frogr.desktop file.
  * Update my e-mail address in debian/*.

 -- Alberto Garcia <berto@igalia.com>  Sun, 24 Nov 2013 20:15:42 +0200

frogr (0.8-3) unstable; urgency=low

  * Don't make builds fail because of deprecated API (Closes: #707379).
    - allow-deprecated-api.patch: remove -DG_DISABLE_DEPRECATED and
      friends from CFLAGS.
    - debian/control: add dependency on dh-autoreconf.
    - debian/rules: run dh --with autoreconf
  * debian/copyright: update copyright years.

 -- Alberto Garcia <agarcia@igalia.com>  Thu, 09 May 2013 15:36:34 +0300

frogr (0.8-2) unstable; urgency=low

  * Upload to unstable.
  * debian/control: remove obsolete DM-Upload-Allowed flag.
  * debian/control: update Standards-Version to 3.9.4 (no changes).

 -- Alberto Garcia <agarcia@igalia.com>  Sun, 05 May 2013 21:05:29 +0300

frogr (0.8-1) experimental; urgency=low

  * New upstream release.
  * Remove debian/preferences-general.png, it's already included in this
    release.
  * debian/control: update build dependencies:
    - libgstreamer0.10-dev and libjson-glib-dev are now required.
    - GTK+ 2 is no longer supported. The minimum GTK+ version is 3.4.
    - Depend on libglib2.0-dev >= 2.32.
  * debian/control: recommend gstreamer0.10-plugins-base,
    gstreamer0.10-plugins-good and gstreamer0.10-ffmpeg.
  * debian/control: suggest gstreamer0.10-plugins-bad and
    gstreamer0.10-plugins-ugly.
  * debian/control: depend on shared-mime-info.
  * debian/copyright: update upstream e-mail address.
  * debian/docs: remove TODO file.

 -- Alberto Garcia <agarcia@igalia.com>  Tue, 01 Jan 2013 16:55:55 +0100

frogr (0.7-2) unstable; urgency=low

  * debian/preferences-general.png: this file was missing from the tarball
    (and the repository). Fixed upstream in 101ea816e0e4d5c42609a71c8ebe5.

 -- Alberto Garcia <agarcia@igalia.com>  Fri, 25 May 2012 12:32:06 +0200

frogr (0.7-1) unstable; urgency=low

  * New upstream release.
  * debian/{compat,control}: set debhelper compatibility level to 9.
  * debian/copyright: rewrite using the machine-readable format.
  * debian/control: update Standards-Version to 3.9.3 (no changes).
  * debian/control: add build dependency on libgcrypt11-dev.
  * debian/watch: scan for .xz files, upstream no longer uses bz2.
  * Split package into frogr and frogr-data:
    - Add frogr.install and frogr-data.install files.
    - Add section for the frogr-data package in debian/control.

 -- Alberto Garcia <agarcia@igalia.com>  Tue, 22 May 2012 10:15:53 +0200

frogr (0.6.1-1) unstable; urgency=low

  * New upstream release.
  * debian/watch: point to ftp.gnome.org.
  * Drop all patches, since they are already included in this release.

 -- Alberto Garcia <agarcia@igalia.com>  Sat, 20 Aug 2011 17:50:43 +0300

frogr (0.6-1) unstable; urgency=low

  * New upstream release.
  * Drop these patches, since they have already been merged upstream:
    - 01_fix-cmdline-segfault.patch (upstream 044d929b389863924a87f56660b)
    - 02_update-manpage.patch (upstream 3f4c935fd89738f0b49a7dd06ee3cfea9)
  * New patch to fix a problem with GtkComboBox in old versions of GTK+:
    - 02_gtk-combo-box.patch (upstream bdfb127fe55d and 2cdf887f50c1e213b)
  * debian/control: minimum required libgtk2.0-dev version is now 2.16.
  * debian/control: minimum required libsoup2.4-dev version is now 2.26.
  * debian/control: add build dependencies on libsoup-gnome2.4-dev,
    gnome-doc-utils and libglib2.0-dev (>= 2.22).
  * debian/control: recommend yelp to browse the online help.
  * debian/copyright: fix copyright years.

 -- Alberto Garcia <agarcia@igalia.com>  Sat, 13 Aug 2011 12:09:56 +0300

frogr (0.5-1) unstable; urgency=low

  * New upstream release.
  * Drop these patches, since they have already been merged upstream:
    - 01_remove-valist-check.patch (upstream e9fe1843341d2ad905418f843bbf)
    - 02_dont-use-werror.patch (upstream 4b948ee6729e0a15f600cdbc5887a93b)
  * New patches to fix a command-line parsing problem:
    - 01_fix-cmdline-segfault.patch (upstream 044d929b389863924a87f56660b)
    - 02_update-manpage.patch (upstream 3f4c935fd89738f0b49a7dd06ee3cfea9)
  * 03_potfiles-quilt.patch (make intltool skip quilt's .pc directory)
  * debian/rules: switch from cdbs to dh.
  * debian/control: remove dependency on cdbs.
  * debian/control: add build dependency on libexif-dev.
  * debian/control: compile with GTK+ 3 by default, add build dependency
    on libgtk-3-dev but keep libgtk2.0-dev as an option.
  * debian/control: update Standards-Version to 3.9.2.
  * debian/docs: add files from the former DEB_INSTALL_DOCS_ALL variable.

 -- Alberto Garcia <agarcia@igalia.com>  Fri, 27 May 2011 10:02:41 +0200

frogr (0.4-3) unstable; urgency=low

  * 02_dont-use-werror.patch: Fix FTBFS (Closes: #625336)

 -- Alberto Garcia <agarcia@igalia.com>  Tue, 03 May 2011 15:01:23 +0300

frogr (0.4-2) unstable; urgency=low

  * debian/{copyright,control,watch}: point URLs to gnome.org
  * 01_remove-valist-check.patch: Fix FTBFS (Closes: #613905)

 -- Alberto Garcia <agarcia@igalia.com>  Fri, 18 Feb 2011 11:40:23 +0200

frogr (0.4-1) unstable; urgency=low

  * New upstream release
  * Drop 01_pixmapdir.patch, already merged upstream (8a686efba0fb07089fc)
  * Drop debian/frogr.1, already included upstream (d927137ae629b4c5de149)
  * debian/copyright: add 2011
  * debian/control: remove redundant dependency on libglib2.0-dev and
    increase minimum required version of libgtk2.0-dev to 2.14
  * debian/control: add DM-Upload-Allowed flag

 -- Alberto Garcia <agarcia@igalia.com>  Mon, 07 Feb 2011 12:48:50 +0100

frogr (0.3-1) unstable; urgency=low

  * Initial release (Closes: #607889).
  * 01_pixmapdir.patch: Install XPM file in /usr/share/pixmaps

 -- Alberto Garcia <agarcia@igalia.com>  Thu, 23 Dec 2010 19:10:52 +0100
